# Subnetting: Herr des Rings

### Arbeitsauftrag:
- Hands-on Auftrag mit Filius. In diesem Auftrag verschieben wir die Subnetzmaske in das letzte UND vorletzte Oktett der IP-Adresse. Hilf Frodo seine Freunde (und Feinde) zu finden. Sie verstecken sich in nahen kleinen - und weiten grossen - Subnetzen...


### Vorgehen:

**1. Auftrag, Excel- & Filiusfile downloaden**

- Downloade den vorbereiteten  [Arbeitsauftrag](m117_Herr-des-Rings_INLINE_FIELDS.pdf)
- Downloade das vorbereitete [Excel-File](m117_Subnetting_Herr-des-Rings.xlsx)
- Downloade das vorbereitete [Filiusfile](Filius-Alle-IPs-konfiguriert.fls)
- Öffne den "runtergeladenen" [Arbeitsauftrag](m117_Herr-des-Rings_INLINE_FIELDS.pdf) mit einem PDF-Reader (Inline-Felder zum ausfüllen)

**2. Auftrag durchführen**
 - `Arbeitsauftrag` durchlesen. **Achtung!** Auf der ersten Seite wird das Vorgehen erklärt.  
 - `Excel-File` öffnen und bearbeiten.
 - `Filius-File` öffnen. 
 - Anschliessend der Reihe nach die Fragen im Auftrag beantworten

**3. Auftrag abgeben**
 - PDF-File als `DeinNachname_Herr-des-Rings.pdf` gemäss Angaben der Lehrperson abgeben


<br>


---

> [⇧ **Zurück zu N2**](../../../README.md)

---


> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---