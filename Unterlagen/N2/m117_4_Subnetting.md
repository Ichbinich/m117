# Subnetting

Netze setzen sich meistens aus mehreren Segmenten (Teilabschnitten) zusammen.
Eine Telefonnummer besteht beispielsweise aus einer Vorwahl und einer Teilnehmernummer. Eine **IP-Adresse** ist ähnlich aufgebaut und besteht aus den folgenden zwei Teilen, wobei die
Abkürzung **ID** für Identifikation steht:

- Die **Netzwerk-ID** (bzw.Netz-ID) im vorderen linken Teil entspricht der Vorwahl und kennzeichnet das entsprechende Netzwerk.
- Die **Host-ID** im hinteren rechten Teil kennzeichnet eine einzelne Netzwerk-Schnittstelle und
entspricht der Teilnehmernummer im Ortsnetz. Bei einer **Netzwerk-Adresse** enthält sie nur Nullen.

Eine IP-Adresse kann zudem, sehr ähnlich wie eine Telefonnummer, "gezügelt" werden. Die MAC-Adresse einer Netzwerkkarte ist diesbezeglich eher vergleichbar mit einer Wohnadresse. Wenn man zügelt, kann man die Tel.Nr. "mitnehmen" - die Wohnadresse ändert sich dann allerdings (die alte Wohnadresse bleibt fix wo sie ist).

Entsprechend können Rechner im selben Netzwerk direkt miteinander kommunizieren. Dagegen erfordert Kommunikation **zwischen Netzwerken** einen **Vermittler** (Router oder Gateways). Auf den Rechnern ist dafür die Adresse des Vermittlers als Standardgateway (auch Default-Route) angegeben. Um zu erkennen, wo die Netzwerk-ID endet und die Host-ID beginnt, muss bei der Bezeichnung von Netzwerken zusätzlich zur IP-Adresse zwingend eine sogenannte **Subnetzmaske** angegeben werden. Diese legt die Größe eines Netzwerk-Segments fest.

## Subnetzmaske

Die Subnetzmaske ist ein Bitmuster, das (von links nach rechts) Teile der IP-Adresse maskiert, um den Übergang zwischen Netz-ID und Host-ID zu kennzeichnen. Binär betrachtet besteht eine Netzmaske aus einer Folge von Einsen, die ab einer bestimmten Stelle zu einer Folge von Nullen wechselt. Dieser Wechsel gibt an, wie viele Bits zur **Netzwerk-ID** (Einsen) und zur **Host-ID** (Nullen)
gehören. Die Schreibweise erfolgt dabei ebenfalls im Dotted-Decimal-Format. 

Hier ein Beispiel für die IP-Adresse **192.168.0.1** und der Subnetzmaske **255.255.255.0**:

![subnetzmaske](images/01-ip-und-netz_800.png)

Die Maskierung ergibt, dass sich der Computer mit der IP-Adresse 192.168.0.1 im Netzwerk (Net-ID) **192.168.0** befindet und die Nummer (Host-ID) **1** besitzt.

<br>

**Netzmasken** werden oft auf der Basis von ganzen Oktetten gesetzt, was die folgenden Beispiele zeigen:

![netzmasken](images/02-3ips_800.png)


- Eine **Netzwerk-Adresse** stellt die erste Adresse in einem Netzwerk-Segment dar. Die Host-ID besteht dabei nur aus Nullen. Die letzte Adresse ist die **Broadcast-Adresse**, deren Host-ID nur Einsen enthält.
- Ein **Netzwerk** wird durch die Netzwerk-Adresse **zusammen** mit der **Netzmaske** definiert. In einem IPv4-Netzwerk kennzeichnet die Netzwerk-Adresse die erste IP-Adresse, die Netzmaske legt dessen Größe fest.
- Im Grunde ist auf IP-Ebene jedes andere Netzwerk ein Subnetz (Erläuterungen siehe unten) des Internets.

## IP-Adressklassen

Früher wurden zu vergebende IP-Adressen in verschiedene Adressklassen aufgeteilt. Für die
Adressierung von Rechnern waren die Klassen A, B und C vorgesehen. Heute ist die klassenbasierte Vergabe vom **C**lassless **I**nter-**D**omain **R**outing (**CIDR)** abgelöst. 

Die nachfolgende Tabelle zeigt die Aufteilung:

![classes](images/03-classes_800.png)

Einige Adressbereiche dürfen **nicht** vergeben werden:
- die mit **0** beginnen (wegen der Default-Route)
- die mit **127** beginnen (sie sind reserviert für interne Funktionen, wie z.B. die **Loopback**-Adresse 127.0.0.1, die auf jedem Rechner konfiguriert ist und die eigene
Adresse für das virtuelle Netzwerkinterface des IP-Stacks darstellt)
- die mit **255** beginnen (für Broadcasts).

Jedes IPv4-Netzwerk verwendet die erste Adresse (0) für die Adressierung des Netzwerks (Netzwerk-Adresse) und die letzte Adresse (255) als **Broadcast-Adresse**. Ein **Class-C-Netz** kann daher keine 256, sondern **nur 254 Hosts** beinhalten.

## Subnetze (Subnetting)

Die Netzmaske kann zur Aufteilung bestehender Netzwerke in Subnetze benutzt
werden (auch **Subnetting** genannt). Das größte Netz ist das Internet selbst, welches über Netzmasken in viele weitere Netzwerke aufgeteilt wird. Das **gesamte Internet** hat die Netzwerk-Adresse **0.0.0.0** und die Netzmaske **0.0.0.0**. Diese Angaben sind Teil der **Default-Route**, die in der Regel den Weg zu demjenigen Router zeigt, der die Verbindung eines Netzwerks (oder Subnetzes) zu allen vorkommenden IP-Adressen (also dem Internet) ermöglicht. Weitere Details dazu werden sie im Modul 129 kennenlernen.

Soll etwa das bestehende Netz mit der Netzwerk-Adresse **195.10.1.0** in **2 Teilnetze** segmentiert werden, so kann die Netzmaske um 1 Bit erweitert werden. Dieses Bit geht dann für die Adressierung von Host-IDs verloren. Das Resultat wären **zwei** Netzwerk-IDs, bestehend aus je 25 Bits mit der zugehörigen Netzmaske **255.255.255.128**. Das folgende Wandtafelbild zeigt die entsprechende Unterteilung.

![classes1](images/07a_2_Subnetze_400.jpg)

Die nächste Tabelle gibt die möglichen IP-Adressen für die **zwei Subnetze** an und zeigt den binären Aufbau (Subnetze in CIDR-Notation):

**1. Segment** (Rechte Hälfte des Kreises, siehe Bild oben)

![classes2](images/04_25er_1_800.png)

**2. Segment** (Linke Hälfte des Kreises, siehe Bild oben)

![classes3](images/04_25er_2_800.png)


Das bestehende Netz mit der Netzwerk-Adresse **195.10.1.0** könnte somit auch in 
- **4 Teilnetze**
- **8 Teilnetze**
- **16 Teilnetze**

segmentiert werden, so kann die Netzmaske um 1 Bit erweitert werden. Die Netzmaske muss dann jeweils **um ein weiteres Bit erweitert** werden. Bei jeder dieser Erweiterungen geht dann allerdings ein Bit für die Adressierung von Host-IDs verloren (wird also jedesmal halbiert)

<br>

**4 Teilnetze** (mit 2 Bits) |  **8 Teilnetze** (mit 4 Bits)
:---:|:---:|
![](images/07b_4-Subnetze_400.jpg)    |  ![](images/07c_8-Subnetze_400.jpg) |  ![](images/07c_8-Subnetze_400.jpg)

<br>

Folgend wird noch das Beispiel **oben links (4 Teilnetze / Segmente)** anhand des binären Aufbau's dargestellt (Subnetze in CIDR-Notation):

**1. Segment** (Violettes Viertel des Kreises, rechts oben)

![classes](images/05_26er_1_800.png)

**2. Segment** (Blaues Viertel des Kreises, rechts unten)

![classes](images/05_26er_2_800.png)

**3. Segment** (Gelbes Viertel des Kreises, links unten)

![classes](images/05_26er_3_800.png)

**4. Segment** (Rotes Viertel des Kreises, links oben)

![classes](images/05_26er_4_800.png)


## LB1-Vorbereitung

Das folgende Aufgabenblatt beinhaltet diverse Fragen, die in einer ähnlichen Form auch an der **LB1** gestellt werden

- ![LB1 Vorbereitungsaufgaben](ressourcen/M117_Aufgaben_LB1-vorbereitung.pdf)


<br>


---

> [⇧ **Zurück zu N2**](README.md)

---

> [⇧ **Zurück zu Unterlagen**](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---
