# Subnetting im letzten Oktett

### Arbeitsauftrag:
- In diesem Auftrag ist jeweils die IP-Adresse und die CIDR bekannt. Aufgrund dieser Vorgaben sollen folgende Kenngrössen ermittelt werden:
    - Netzwerk-ID
    - Erste IP-Adresse des Subnetzes
    - Letzte IP-Adresse des Subnetzes
    - Broadcast-Adresse des Subnetzes

Je weiter sich die Netzwerkmaske nach rechts ausdehnt, desto kleiner wird der IP-Pool für die Hosts. Das geht soweit, dass gegen Ende dieses Auftrages die immer gleichbleibenden IP-Adressen nicht mehr im gleichen Subnetz sind.

Die Grösse der Subnetzmaske kann also entscheidend sein, ob gleichbleibende IP-Adressen miteinander kommunizieren können - oder eben nicht.


### Vorgehen:

- Erhebe sämtliche Informationen, die Du später für die Konfiguration benötigst (z.B. Netzwerk-IDs, Default-Gateway etc...)


**1. Excel-File bereitstellen**

- Lade das vorbereitete  [Excel-File](ressourcen/IP%2BNetzwerke_CIDR24-30.xlsx) runter

- Öffne das "runtergeladene" File mit Excel oder z.B. LibreOffice


**2. Unten erste Lasche anklicken**

Klicke auf den ersten Tab unten links (01_IP-Adresse+CIDR24) und scrolle ganz nach oben

[![Excel-Lasche](images/40_subnet_600.png)



**3. Felder ausfüllen**

Fülle die leeren Felder aus _(Tipp: von rechts nach links):_
 
 - Beginne rechts mit "Anzahl Hostbits und Hosts" _(Pfeil 3_)
 - Fülle danach die Felder in der Mitte aus _(Pfeil 2_)
 - Berechne **Netmask**, **Network-ID**, **First IP**, **Last IP** und **Broadcast** indem Du die Bits jeder zugeordneten Zeile zusammenzählst _(Pfeil 1)_
 

[![Leere-Felder](images/41_subnet_600.png)

**Beispiel :**
Hier die Lösung der ersten IP-Adresse unter CIDR 24

[![Leere-Felder](images/42_subnet_Loesung1_600.png)

<br>


---

> [⇧ **Zurück zu N2**](README.md)

---

> [⇧ **Zurück zu Unterlagen**](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---