# Inhaltsverzeichnis

[TOC]

# Topologien

Unter einer Netzwerk-Topologie versteht man die physikalische Anordnung von
Netzwerk-Stationen, die über Kabel oder Funk miteinander verbunden sind. Sie
bestimmen die einzusetzende Hardware, sowie die Zugriffsmethoden. Dieses
wiederum hat Einfluss auf das Medium (z. B. das Kabel), auf die
Übertragungsgeschwindigkeit und den Durchsatz der Daten.

<br>

## Stern-Topologie

![stern](images/1.png)

Bei einer Stern-Topologie
wird jede einzelne Station über je ein eigenes Kabel mit einem **zentralen
Verteiler** verbunden. Es handelt sich im Regelfall um einen **Switch** (alt:
Hub). Die einzelnen Stationen müssen sich über ein Protokoll miteinander
verständigen. Der Switch ist in der Lage alle Stationen miteinander zu
verbinden. Dazu werden die Datenpakete auf elektronischem Weg entgegengenommen
und an das Ziel weitergeleitet.

Das Netzwerk funktioniert so lange, bis die Zentralstation ausfällt. Eine
Stern-Topologie ist **leicht erweiterbar**, und **einfach zu pflegen**.

<br>

## Baum-Topologie (Stern-Stern)

![stern-stern](images/2.png)

Ein Stern-Stern-Netz
entsteht, wenn verschiedene Switches jeweils das Zentrum eines Sterns bilden und
diese Switches wiederum über eigene Kabel mit einem Haupt- Switch verbunden
sind. An diesen Haupt-Switch werden in der Praxis dann oft auch wichtige Server
direkt angeschlossen.

Ein einfaches Beispiel: In einem dreistöckigen Bürogebäude ist jedes Stockwerk
in Stern-Topologie verkabelt. Die drei Stockwerke, genauer gesagt: die Switches
untereinander, werden über je ein Kabel mit einem zentralen Haupt- Switch
verbunden.

<br>

## Bus-Topologie

Die Bus-Topologie ist gekennzeichnet durch ein einzelnes zentrales Kabel (Medium), das als **Bus** bezeichnet wird.

Einzelnes, zentrales Kabel |  WLAN (ist auch eine Bus-Anwendung)
:-------------------------:|:-------------------------:
![bus](images/3.png)    |  ![wlan](images/30_WLAN.png)

An diesen Bus werden alle Geräte angeschlossen und müssen sich
dieses Medium **teilen** (shared media).

Die angeschlossenen Stationen greifen die Signale vom Kabel ab oder senden auf
das Kabel, wo sich das **Signal dann in beide Richtungen ausbreitet**. Senden
zwei Stationen gleichzeitig ihre Daten, **kollidieren** die Signale und die
Daten werden **zerstört.** In einer Bustopologie **darf darum nur** **eine
Station senden!**

Im heutigen Local Area Netzwerk (LAN) finden wir keine Bus-Topologien mit Kabeln
mehr, sie sind durch Stern-Topologien ersetzt worden.

**Achtung**: 
Die Problematik der Bus-Topologie stellt sich in gleicher Form im **WLAN**. Jeder Access-Point (AP) bildet eine eigene Bus-Topologie. Auch hier darf nur eine Station senden, sonst gibt es Datenkollisionen mit zerstörten Daten.  
Damit teilen sich alle WLAN-Teilnehmer die Bandbreite des WLAN – und das
Netzwerk wird systembedingt langsam!

<br>



## Vermaschte Topologie

![masche](images/5.png)

In einem (teil-)
vermaschten Netz existieren zwischen den einzelnen Netzknoten jeweils **mehrere
Verbindungen**.   
Sinn dieser Vernetzung ist es, bei **Ausfall** einer Verbindung auf eine zweite,
**redundante** Verbindung zurückgreifen zu können.   
Häufig ist diese Art des Netzaufbaus in Weitverkehrsnetzen zu finden, weniger im
LAN.  
Das **Internet** stellt ein solches Maschen-Netzwerk dar.

<br>

## Gemischte Topologien

![sternbus](images/6.png)

Topologien können auch
nach Bedarf gemischt werden. Hier eine gemischt Bus-Stern-Topologie.

<br>

## Vor- und Nachteile der 4 Grundtopologien:

| **Topologie**| **+ Vorteile** | **- Nachteile**  |
|--|--|--|
| **Stern-Topologie** | Der Ausfall einer Station oder der Defekt eines Kabels hat keine Auswirkungen auf das restliche Netz.  | Große Kabelmengen |
||Aktive Verteiler wirken gleichzeitig als Signalverstärker.|Kosten für den Switch als zentrale Komponente|
||Weitere Stationen und/oder Verteiler können problemlos hinzugefügt werden.|Beim Ausfall des Verteilers ist kein Netzverkehr mehr möglich.|
| **Bus-Topologie** | einfach installierbar, einfach erweiterbar, wenig Kabel | **nur eine Station darf senden**,  Netzausdehnung begrenzt, bei Kabelbruch fällt Netz aus, aufwendige Zugriffsmethoden |
| **Vermaschte Topologie** | Hohe Ausfallsicherheit | aufwendige Administration, teure und hochwertige Vernetzung |


**Hinweis:** Im verkabelten LAN (Local Area Network) ist heutzutage fast ausschliesslich die Sterntopologie anzutreffen.   

Das häufig (zusätzlich) im LAN vorhandenen WLAN ist aber eine Bustopologie – mit allen Nachteilen…

<br>

# Kabel, Verkabelungsart, Ethernetvariante

Die einzelnen Computer müssen verbunden werden. Dies geschieht heute meist mit
Kupferkabel, bei dem je zwei Adern miteinander verdrillt sind, sogenanntes
Twisted-Pair-Kabel (TP). Damit die Datenübertragung zwischen den Computern
funktioniert, müssen neben Stecker und Kabel auch verschiedene Regeln der
Kommunikation festgelegt sein. Diese **Regeln** und die benötigen **Kabel** und
**Stecker** werden durch **Übertragungstechnologie Ethernet** definiert.

## Ethernet

Ethernet ist eine Technologie zur Übertragung von Daten in einem lokalen Netz
(LAN), d.h. in einem Netz mit einer maximalen Grösse von einigen 100m. Wir
möchten jetzt etwas tiefer hinter die Kulissen schauen und verstehen lernen, was
Ethernet bedeutet, welche Varianten davon es gibt und welche Eigenschaften und
Grenzen es hat.

Denn, damit ein Computer Datenpakete an einen anderen Computer am selben Switch
schicken kann, braucht es mehr als nur ein Kabel:

-   Es muss geregelt werden, wer und wann Daten über das Netz schicken darf,
    damit nicht alle gleichzeitig senden.

-   Die Geschwindigkeit beider Enden muss gleich sein.

-   Die einzelnen Bits der Daten müssen zu Päckchen von fester Grösse
    zusammengefasst werden.

-   Die einzelnen Stationen im Netz müssen mit Adressen identifizierbar und
    unterscheidbar sein.

### Geschichte

Ethernet wurde in den 70er Jahren von der Firma Xerox entwickelt, um für einige
wenige Stationen in einem Netzwerk die Verbindungen herzustellen. Zur gleichen
Zeit war Token Ring von IBM eine weit verbreitete Übertragungstechnologie. Heute
ist Token Ring praktisch nicht mehr im Einsatz, so das Ethernet keine
eigentliche Konkurrenz mehr hat. Das heisst, zurzeit wird Ethernet in \>99%
aller LANs (lokale Netze) eingesetzt und ist deshalb für die Praxis das mit
Abstand wichtigste LAN-Übertragungsprotokoll.

### Geschwindigkeit

Die Übertagungsgeschwindigkeit wird in **bps** (**bits per second**) angegeben
(Achtung: **Nicht Byte** per second). Für grössere Geschwindigkeiten werden die
normalen Vorsätze verwendet **k**=kilo, **M**=Mega, **G**=Giga, etc.  
**Mbps** heisst darum „**M**ega**b**it **p**er **s**econd“, was eine
Datenübertragung in Millionen Bit pro Sekunde bedeutet.

Vergleichbar mit einer Autobahn, ist diese Geschwindigkeit nur die
Reisegeschwindigkeit. Ob die Ein- und Ausfahrten diese Geschwindigkeit auch
ausnützen können (Flaschenhals bei Netzwerkkarten oder Festplatten), ist eine
andere Frage.

Während 100Mbps lange Zeit mit verschiedenen Kabeltypen verwendet wurde, ist für
normale PCs zurzeit 1000Mbps (1 Gbps) der Standard. Grösser als 1Gbps (2.5Gbps,
5Gbps, 10Gbps) wird bei Verbindungen zwischen Switches oder bei Servern
verwendet. 10Gbps und höher wird bei Hochleistungsservern und auf dem Backbone
(Kernteil des Netzwerkes) von grossen LANs verwendet.   
Die Datenübertragungs-Geschwindigkeiten, wird auch als **Bandbreite**
bezeichnet.

**Achtung**: Datenübertragungs-Geschwindigkeiten haben die Einheit MBit**/s**
oder Mb**ps** nicht nur Mbit, wie in der Physik nach der Formel Geschwindigkeit
v = s / t!

**Nochmals Achtung**: Datenübertragungs-Geschwindigkeiten werden in **Bit/s** nicht in Bytes/s angegeben, was immer wieder zu Rechenfehler führt (8 Bit/s = 1 Byte/s)

### Twisted-Pair Verkabelung (Ethernet)

![TP](images/7.png)

Normale Arbeitstationen werden
heute mit **Twisted-Pair-Kupferkabeln** (TP = verdrillt) mit **RJ45-Steckern**
an einen Switch angeschlossen. Die maximale Länge eines TP-Kabels beträgt **100m**. In den meisten Fällen genügt diese Länge, weil ein Kabel immer von einem zentralen Punkt (Switch) auf die einzelnen Geräte (Arbeitsstationen, Server) gezogen wird.

Bei TP-Verkabelung sind meist alle 8 Adern (4 Paare) verkabelt, benötigt werden
aber für Ethernet bis 100Mbit/s nur 4 davon (2 Paare), bei GBit-Ethernet werden
alle 8 Adern (4 Paare) eingesetzt.


Bei Ethernet bis **100Mbit/s** werden 4 Adern genutzt

[![TP](images/8-Gross.png)

<br>

Bei **GBit-Ethernet** werden alle 8 Adern genutzt


Belegung der Adern |  RJ45-Anschlussstecker
:-------------------------:|:-------------------------:
![](images/9.png)    |  ![](images/10.jpeg)


<br>

----

## Lichtwellenleiter (Glasfaser) Verkabelung (Ethernet)

LC Connector |  SC Connector
:---:|:---:
![](images/11.png)    |  ![](images/12.png)


Neben den Twisted-Pair-Kupferkabeln können auch **Lichtwellenleiter LWL** (auch Glasfaser, Fiber genannt) eingesetzt werden. Hier erfolgt die Datenübertragung nicht mit
elektrischem Strom sondern mit Lichtimpulsen über eine sehr dünne Faser aus Glas
übertragen und ist darum gegen elektromagnetische Störungen unempfindlich.

![LWL](images/13.png)

**LWL** bestehen tatsächlich aus Glas. Die Fasern können entweder mittels Steckverbinder
gesteckt werden oder die Fasern werden mit einem Lichtbogen fast verlustfrei
gespleisst (zusammengeschmolzen)

Grundsätzlich werden zwei Fasertypen unterschieden, die **Mono Mode Faser**
(auch **Singlemode** genannt) und die **Multi Mode Faser**. Mit unserem Auge
können wir den Unterschied dieser beiden Fasertypen nicht feststellen.

Äusserlich sehen diese Fasern auch genau gleich aus. Beide Fasertypen haben
einen Aussendurchmesser von 125 μm (0.125 mm) ohne Umhüllung, respektive 250 μm
mit Umhüllung.

![](images/14.gif)

Bei **Multi Mode** Faser (Kerndurchmesser 62,5 μm bzw. 50 μm) wird das Licht vom
Fasermantel abgelenkt und legt so einen relativ langen Weg zurück bis zum
Empfänger.

Das Licht in der **Single-** oder **Mono Mode** Faser (Kerndurchmesser 3 μm bis
9 μm) gelangt auf direktem Weg vom Sender zum Empfänger.

Mono Mode Fasern können Daten bis zu 40 km ohne Verstärkung übertragen.
Multimode Fasern können nur für kürzere Strecken eingesetzt werden. Der Grund
ist die Lichtausbreitung in der Faser.

Zum Einsatz kommen LWL insbesondere bei

-   **elektromagnetischen Störungen**  
    LWL sind sehr unempfindlich gegen elektromagnetische Störungen

-   und bei Verbindungen **die länger als 100m** sind.  
    mit LWL, können bedeutend grössere Distanzen überwunden werden als mit
    TP-Kupferkabel

Früher waren LWL auch leistungsfähiger (schneller) als TP-Kupferkabel, heute
sind diese hohen Geschwindigkeiten aber oft auch mit TP-Kupferkabel
realisierbar.  
Generell werden LWL auch bei Verbindungen **zwischen verschiedenen Gebäuden**
verwendet, um elektrische Problem zu verhindern.

<br>

# Verkabelung: Fliegend oder universell?

In einer kleinen Firma kann die Verkabelung „fliegend“ sein, d.h. die einzelnen
Kabel werden direkt mit einem Kabel vom zentralen Gerät (Switch) an der
Arbeitsstation angeschlossen. Diese Kabel müssen dann flexibel im wirklichen
Sinn des Wortes sein. Ihre Adern bestehen aus **Litzen** (nicht aus Drähten),
d.h. sie sind aus 20-40 sehr dünnen verdrillten Kupferleiter gefertigt statt aus
einem dicken Draht. Deshalb sind sie weich und biegsam, ohne dabei die gebogene
Form zu behalten wie Drähte.


## Universelle Gebäudeverkabelung (UGV)

Die Strukturierte Verkabelung, auch als Universelle Gebäudeverkabelung (UGV)
oder Universelle Kommunikationsverkabelung (UKV) bezeichnet, ist ein Konzept für
die Verkabelung mit anwendungsneutralen Kommunikationskabeln in und zwischen
Gebäuden

Für grössere Netze und in neuen Gebäuden verlegt man die Leitungen **fest** z.B.
in Kabelkanäle, in Brüstungen, Zwischendecken, Doppelböden. Dadurch sind die
Kabel geschützt. Bei einer festen Verkabelung wird an beiden Enden anstatt eines Steckers eine Buchse montiert. Dadurch werden aber an beiden Enden zusätzliche
Kabel zu den Geräten nötig.

Diese Installationsart erlaubt eine saubere und langlebige Verkabelung. Die
Verkabelung ist universell einsetzbar, sie kann für verschieden Technologien
(meist Ethernet) genutzt werden, je nachdem, was Sie an den Enden der Leitungen
anschliessen.

Bei der UGV verwendet man im Gegensatz zur fliegenden Verkabelung starre,
massive **Drähte** an deren Enden Buchsen in einer Anschlussdose montiert
werden. Die maximale Länge eines TP-Kabels ist bei Ethernet auf 100m begrenzt.
Bei einer solchen Installation geht man von ca. 90m aus und lässt die 10m für
die fliegende Verkabelung von der Anschlussdose zum PC und auf der anderen Seite vom Patchpanel zu den Anschlussgeräten wie ISDN S-Bus, Ethernet-Switch.

Querschnitt |  UGV (Draht, starr)
:-------------------------:|:-------------------------:
![](images/15.png)    |  ![](images/15_Draht.jpg)

Querschnitt |  Fliegend (flexibel)
:-------------------------:|:-------------------------:
![](images/16.png)    |  ![](images/16_Litze.jpg)

(Abgebildet ist je ein Leiter, d.h. eine Ader von 8 in einem TP-Kabel)

<br>

Anschlussdose Aufputz (AP) |  Anschlussdose Unterputz (UP)
:-------------------------:|:-------------------------:
![](images/17.png)    |  ![](images/18.png)

<br>

# Geschirmt oder nicht geschirmt....

Die Twisted–Pair Kabel werden in zwei Ausführungen geliefert:

UTP (Unshielded Twisted-Pair) |  STP (Shielded Twisted-Pair)
:-------------------------:|:-------------------------:
[![utp](images/19-UTP.png)](images/19-UTP_Gross.png)    |  [![utp](images/20-STP.png)](images/20-STP_Gross.png)


Unterarten davon sind S/UTP, S/STP, F/UTP und FTP worin das erste S/ für
Screened steht und F/ für Foiled (Foil = Folie). Der Screen/Foil ist eine
zusätzliche Abschirmung um das ganze Kabel, nicht nur um die Adernpaare.

**UTP** (Unshielded Twisted-Pair):<br>
UTP-Kabel sind **anfälliger gegenüber elektromagnetischen Störfeldern**. Neben möglichen
Einstrahlungen von außen, z. B. wenn in einem Kabelschacht mehrere Kabel eng beieinander
liegen, ist ein weiteres Problem das Übersprechen zwischen den Adernpaaren eines Kabels,
während der Übertragung von sehr hohen Frequenzen. UTP-Kabel ermöglichen dagegen einen
geringeren Verlegeaufwand und ein aufwendiger Potenzialausgleich für die Schirmung entfällt.

**STP** (Shielded Twisted-Pair):<br>
Shielded (geschirmt) bedeutet, dass **jedes
Adernpaar einzeln abgeschirmt** wird. Das
Kabel insgesamt (alle Adernpaare) wird durch
eine Ummantelung mechanisch geschützt.
Dadurch sind STP-Kabel **gegenüber elektrischen
Störeinflüssen weniger anfällig** als
UTP-Kabel.

Welches Kabel verwenden? Während die Befürworter des STP mit der besseren Abschirmung gegen Störungen argumentieren, führen die Vertreter des UTP die
geringeren Kosten und die mechanische Anfälligkeit der Abschirmung bei STP an.

Auf der Stufe des Moduls 117 ist dieses Thema eher zweitrangig. Lehnen sie sich
an die Erfahrungen ihres Lehrbetriebes.


# Kabelkategorien

Die Kabelkategorie ist ein Mass für die maximale Frequenz mit der ein
Twisted-Pair-Kupferkabel elektrische Signale übertragen kann. Für eine schnelle
Datenübertragung ist eine möglichst hohe maximale Übertragungsfrequenz nötig.  
Die Übertragungsfrequenz wird durch die verwendeten Materialien und die
mechanische Verarbeitungsqualität des Kabels und vor allem des Steckers
bestimmt. D.h. Kabel mit hoher Übertragungsfrequenz sind aufwendig herzustellen
und somit deutlich teurer.

| **Kategorie** | **Max. Übertragungs frequenz** | **Einsatzgebiet**     |
|---------------|--------------------------------|-----------------------|
| **Cat 1**     | -                              | Telefon, Klingeldraht |
| **Cat 2**     |  1 MHz                         | Telefon               |
| **Cat 3**     |  16 MHz                        | 10BaseT               |
| **Cat 4**     |  20 MHz                        | Token Ring            |
| **Cat 5**     |  100 MHz                       | 100BaseTX             |
| **Cat 5e**    |  100 MHz                       | 1000BaseT             |
| **Cat 6**     |  250 MHz                       | 1000BaseT, Multimedia |
| **Cat 7**     |  600 MHz                       | 10GBaseT              |
| **Cat 8**     |  800 MHz                       | 10GBaseT, Kabel-TV)   |

In heutigen Ethernet-Netzwerken werden **Cat 5e und 6** eingesetzt.   
Für Neuinstallationen sollte mindestens Cat 6 oder besser Cat 7 Kabel verwendet,
damit ist man auch für zukünftige Technologien vorbereitet.

## Varianten (oder Typen) von Ethernet

In der Tabelle finden Sie die wichtigsten **Ethernet-Typen** mit Kenndaten und
Einsatzgebieten.

| **Bezeichnung** | **Geschwindigkeit** | **Kabeltyp,  minimale Kategorie**     | **Max. Reichweite** | **Typische Einsatzgebiete** |
|---|---|---|---|---|
| **100 Base TX**                         | 100 Mbps  | TP,  **Kat. 5**        | 100m  | Standard bei (älteren) Netzwerken. In Verbindung mit Switch ideales Preis- / Leistungsverhältnis. |
| **100 Base FX**  | 100 Mbps   | Glasfaser  | 2000m  | Elektromagnetische Störungen,  Verbindungen zwischen Gebäuden, explosionsgefährdete Zonen, Langstrecken-Verbindungen    |
| **1000 Base T** | 1000 Mbps  | Twisted Pair **Kat. 5e, 6** | 100m                | Aktueller Standard bei neuen Netzwerken: Verbindungen zu PCs, Druckern, zwischen Switches.                              |
| **1000 Base SX** | 1000 Mbps | Glasfaser | 550m | Verbindungen zwischen Switches Langstrecken-Verbindungen                                                                |
| **1000 Base LX**                        | 1000 Mbps                | Glasfaser                             | 5000m               | Verbindungen zwischen Switches Langstrecken-Verbindungen                                                                |
| **10 GBase T** | 10 Gbps  | TP Cat 5e, TP Cat 6, TP Cat 6a (4 Paare)  | 100 m               | In grossen Netzwerken, Verbindungen zwischen Switches, Anschluss an (Hochleistungs-)Server Backbone, schnelle AP (WLAN) |
| **10 GBase SR**                         | 10 Gbps                  | Glasfaser                             | 300m                | (Hochleistungs-)Server Verbindungen zwischen Switches Langstrecken-Verbindungen                                         |

## Praxiseinsatz
Viele **kleinere LANs** verwenden noch **100Base-TX**. Bei **Neuinstallationen** kommt **fast nur noch 1000BaseT (Gigabit) und höher** zum Einsatz. Ältere Geräte mit geringerer Übertragungsrate
werden dabei von einem Switch ebenfalls akzeptiert, sofern die Steckverbindungen zusammenpassen
(nicht RJ-45-Stecker auf Lichtleiter-Buchse und umgekehrt). Die Kabeldistanz zwischen
den Komponenten und dem Switch darf dabei **nicht mehr als 100m** betragen, bezogen auf
Twisted-Pair-Kabel. Bei Lichtwellenleitern ist die Entfernung vom Typ (Singlemode oder Multimode)
abhängig. Jede Station ist dabei dediziert angebunden.


<br>

# Switches (und früher Hubs), die Geräte für Ethernet

Alle Geräte im Netzwerk (PCs, Drucker, Router, …) werden über Ethernet-Kabel an **Switches** angeschlossen und so verbunden.

Switches haben je nach Grösse unterschiedlich viele Anschlüsse, an jedem Anschluss kann ein Gerät (oder ein anderer Switch) angeschlossen werden.  
Es gibt Switches mit wenigen Anschlüssen bis Switches mit dutzenden oder sogar
hunderten von Anschlüssen.

<br>

**Möglicher Einsatz im KMU-Bereich:**

Stern-Topologie |  5-Port Switch
:-------------------------:|:-------------------------:
[![star](images/22.png)](images/22.png)    |  [![switch5](images/23.jpg)](images/23-Gross.jpeg)

<br>

**Möglicher Einsatz im Enterprise-Umfeld:**

Midrange |  Highend (Nexus-Series 7k)
:-------------------------:|:-------------------------:
[![ent-sw1](images/24.jpg)](images/24-Gross.jpeg)    |  [![ent-sw2](images/25.jpg)](images/25-Gross.jpeg)

<br>

**Der Switch** funktioniert wie
ein Schalter (Switch heisst Schalter auf Englisch), der die zwei Station, die
miteinander kommunizieren wollen, **direkt verbindet**.

Dadurch sind alle anderen Stationen noch frei und können ebenfalls an eine
andere Station senden.  
So hat jede Station die volle Bandbreite des Anschlusses (z.B. 1000 Mbit/s) zur
Verfügung.

Ein positiver Effekt dabei ist, dass der Datenverkehr zwischen zwei Geräten, von
anderen am Switch angeschlossenen Geräten **nicht abgehört werden kann**.

![Switch-ia](images/26.gif)

<br>

Den **früher (!) verwendeten
Hub** kann man sich als verkleinerten Bus vorstellen, der sich im Hub befindet. 

Jedes eingehende Datenpaket wird an alle angeschlossenen Stationen gesendet. Das
heisst auch, es darf jeweils **nur eine Station senden**.  
Wenn mehrere Stationen senden wollen, können sie dies **nur nacheinander** und
**teilen** sich damit die Bandbreite des Hubs.  
Bei z.B. drei Stationen ergibt das einen Drittel.   
Damit sinkt die «gefühlte» Geschwindigkeit, das System wird langsamer.

![Switch-ia](images/27.gif)


<br>

# CSMA/CD, das Zugriffsverfahren bei Ethernet

Das Zugriffsverfahren regelt wer wann senden darf auf dem Bus oder am Hub, und
was passiert falls trotzdem zwei Stationen aufs Mal senden.

Bei Ethernet ist jede Station gleichberechtigt. Weder ein Server, noch eine
Person mit Rechten eines Administrators, kann Einfluss auf dieses Verfahren
nehmen. Daher müssen die Stationen aufeinander Rücksicht nehmen: Jede Station
hört den Bus ab, ob er schon besetzt ist und sendet nur, wenn dieser frei ist.
Dieser Teil des Zugriffsverfahrens ist das „Carrier Sense Multiple Access“.
Falls das einmal schief geht, und die Station nicht bemerkt, dass der Bus
besetzt ist, kommt die „Collision Detection“ zum Einsatz. Wenn mehrere Stationen
gleichzeitig senden, nennt sich das eine Kollision, die Daten beider Stationen
sind dann unbrauchbar und müssen noch mal gesendet werden. Wenn eine Kollision
entdeckt wird, hören alle auf zu senden und beginnen nach einer zufälligen Zeit
ihre Daten erneut zu senden.

## Flussdiagramm für CSMA/CD

[![csma-cd](images/28-klein.jpg)](images/28-Gross.jpeg)

Kollisionen benötigen jedes Mal relativ viel Zeit, deshalb sinkt die
Übertragungsleistung eines Netzes, wenn sich darin viele Kollisionen ereignen.
Je mehr Stationen sich in einem Netz befinden, desto höher ist die Chance für
eine Kollision. **Kollisionen** können jedoch **nur bei Hubs** vorkommen, **bei
Switches prinzipbedingt nicht**, auch deshalb sind Switches den Hubs
vorzuziehen.

Praxis: Kabel Installieren und testen:

Laut Verkabelungsplan der Sota GmbH sind einige Geräte im Netzwerk mittels
Direktverbindungen, andere über fest installierte Leitungen verbunden. Diese
beiden verschiedenen Methoden müssen bei der Installation und der Materialbestellung berücksichtigt werden.

<br>

# Anschluss kontrollieren

Wenn die Arbeitsstation mit dem Switch verbunden wird, sieht man bei den meisten
Geräten anhand von Kontrollleuchten, die sogenannten „Link Lights“, ob und mit
welcher Übertragungsrate (100/1000 Mbps) die Verbindung (auf Englisch ein Link)
besteht.

**Linklights** auf Netzwerkkarte und Switch (LED's)

Netzwerk-Adapter |  Switch
:-------------------------:|:-------------------------:
[![Link-lights](images/29.png)](images/29.png)    |  [![link-lights](images/30.jpeg)](images/25-Gross.jpeg)


<br>

# WLAN Grundlagen

Die Netzwerktechnologie **WLAN** (WiFi) wurde Ende der 1990er Jahre entwickelt und ist heute nicht mehr aus Heimnetzwerken wegzudenken. Die WLAN-Technologie ermöglicht es, ein Netzwerk über kurze Distanzen (typisch mehrere 10m innerhalb eines Gebäudes) aufzubauen, welches dann bis zu mehreren dutzend Geräte miteinander verbinden kann. Da als Übertragungsmedium Funk genutzt wird, entfallen aufwendige Kabelinstallationen.

Mit der Verbreitung der Smartphones hat die Anzahl der direkt WLAN fähigen Geräte massiv zugenommen. Doch auch Notebooks, Tablets, Drucker, Überwachungskameras, IoT-Devices, Internet-Router, Spielekonsolen und viele andere Geräte haben ab Werk eine WLAN-Schnittstelle integriert. 

Die verbreitetsten WLAN Varianten nutzen 2 Frequenzbänder. Zu einem das ISM (Industrial, Scientific and Medical) Band bei **2,4GHz**, zum anderen das breitere UNII (Unlicensed National Information Infrastructure) Band bei **5GHz**. Eine neue WLAN Variante (IEEE802.11ad) soll ein Frequenzband bei 60GHz nutzen und dabei sehr hohe Datenraten bieten, über allerdings nur sehr kurze Distanzen.


## CSMA/CA, die Zugriffssteuerung bei WLAN

Der WLAN-Access-Point und sämtliche WLAN-Clients welche auf dem gleiche Frequenzband und Kanal senden und empfangen, teilen sich das gleiche Übertragungsmedium. Das bedeutet demzufolge, dass der Zugriff auf das Übertragungsmedium **Funk** in irgendeiner Art und Weise geregelt sein muss. 


Hierfür wird das sogenannte **CSMA/CA** Konzept (Carrier Sense Multiple Access/Collision Avoidance) verwendet - **CSMA/CD** haben wir bereits weiter oben kennengelert. 

Kern des Konzeptes ist, dass alle Teilnehmer im WLAN lauschen ob das Funkübertragungsmedium frei ist, also kein anderer Teilnehmer gerade sendet. Ist dies für eine definierte Zeit der Fall, wartet der Teilnehmer noch einmal für eine kurze zufällige Zeitdauer, um Probleme zu vermeiden die durch einen wiederkehrenden gleichen Ablauf entstehen könnten. Ist das Funkübertragungsmedium dann immer noch frei, startet der Teilnehmer den Sendeprozess. War der Sendeprozess erfolgreich, dann sendet der Empfänger eine Bestätigung an den Absender und die Datenübertragung war erfolgreich.

Die Wahrscheinlichkeit für Konflikte oder sogenannte Kollisionen auf dem Übertragungsmedium Funk ist hiermit noch nicht gleich Null, aber doch zu einem Großteil vermieden.

Sollte doch einmal eine Kollision entstehen da zufälligerweise 2 Teilnehmer gleichzeitig angefangen haben zu senden, dann wird kein erfolgreicher Empfang stattfinden und somit keine Bestätigung an die Absender geschickt werden. Diese wiederholen dann nach geschildertem Konzept den Sendeprozess solange bis dieser erfolgreich war.

Die Wahrscheinlichkeit für Kollisionen erhöht sich mit der Anzahl von Teilnehmern in einem WLAN. Da jede Kollision dazu führt, dass sich der Sendeprozess wiederholt, steigt die Wahrscheinlichkeit für Kollisionen nicht linear sondern **exponentiell** und die netto Datenrate **sinkt immer schneller gegen Null**.

<br>

## WLAN-Generationen

| **1 Bezeichnung** | **2. Bezeichnung** | **Standard** | **Jahr** | 
|---|---|---|---|
| WLAN 1 | Wi-Fi 1 | IEEE 802.11 | 1999 |
| WLAN 2 | Wi-Fi 2 | IEEE 802.11b | 1999 |
| WLAN 3 | Wi-Fi 3 | IEEE 802.11g | 2003 |
| WLAN 4 | Wi-Fi 4 | IEEE 802.11n | 2009 |
| WLAN 5 | Wi-Fi 5 | IEEE 802.11ac | 2014 |
| WLAN 6 | Wi-Fi 6 | IEEE 802.11ax | 2021 |
| WLAN 6E | Wi-Fi 6E | IEEE 802.11ax (6GHz)| 2022 |
| WLAN 7 | Wi-Fi 7 | IEEE 802.11be | ? |

Leider ist diese Angabe völlig unzureichend. Denn der Standard allein ist nicht der einzige Parameter, der Einfluss auf die Geschwindigkeit und Leistungsfähigkeit der WLAN-Technik hat. So ist die **Anzahl der unterstützten MIMO-Datenströme** und die **Anzahl der benutzten Antennen** genauso wichtig. Damit kann man langsame und schnelle Geräte viel besser voneinander unterscheiden. 

Doch genau diese Angabe wird auf den Verpackungen oft unterschlagen. Ohne Datenblatt und die richtige Interpretation der darin enthaltenden Angaben ist die Beurteilung der möglichen Geschwindigkeit von WLAN-Geräten nicht möglich.
Außerdem hängt die Geschwindigkeit und Leistungsfähigkeit von der Kanalbreite ab, die im Access Point konfiguriert ist.

## Aktuelle WLAN-Standards im Vergleich

| **WLAN Generation** | **Wi-Fi 4** | **Wi-Fi 5** | **Wi-Fi 6 / 6E** | 
|---|---|---|---|
| Maximale Übertragungsrate | 600 MBit/s | 6.9 GBit/s 	 | 9.6 GBit/s |
| Theoretische Übertragungsrate | 300 MBit/s | 870 MBit/s | 1.2 GBit/s |
| Maximale Reichweite | 100m | 50m | 50m |
| Frequenzbereich | 2,4 + 5 GHz | Nur 5 GHz | 2,4 + 5 + 6 GHz |
| Maximale Sende/Empfangseinheiten | 4 x 4 | 8 x 8 | 8 x 8 |
| Antennentechnik | MIMO | (MU-MIMO) | MU-MIMO |
| Maximale Kanalbreite | 40 MHz | 160 MHz| 160 MHz |
| Modulationsverfahren | 64QAM |  	256QAM | 1024QAM |

Wir werden zu einem späteren Zeitpunkt noch etwas weiter in das Thema **WLAN** einsteigen und dieses dann in später folgenden Modulen (M145, 2.Lehrjahr) vertiefen

<br>

## IPv4-Adresse und MAC-Adresse
IP-Adressen in der Version IPv4 sind **32 Bit** lange Binärzahlen (4 Byte), die zur besseren Lesbarkeit im sogenannten **Dotted-Decimal-Format** (oder Dotted-Quad-Notation) angegeben werden: Jedes
der vier Bytes wird als Dezimalzahl mit Werten von **0 bis 255** angegeben, jeweils durch einen
Punkt getrennt (Oktette = Bytes), wie z. B. 193.96.1.200. Die Punkte haben also nichts zu tun mit
dem üblichen Tausendertrennzeichen.

Bei Verwendung von TCP/IP muss jeder Netzwerk-Schnittstelle innerhalb eines Netzwerkverbunds
eine eindeutige IP-Adresse zugewiesen werden. Dadurch wird der physikalischen 48-Bit langen
Hardware-Adresse (MAC-Adresse) eine logische IP-Adresse zugeordnet.

Um ansprechbar zu sein, besitzt jede einzelne Netzwerk-Schnittstelle auf einer Netzwerkkarte
oder auf einem Mainboard (auch bei WLAN) eine **eingebrannte weltweit eindeutige MACAdresse**.
Bei dualband-fähigen WLAN-Geräten haben die Schnittstellen für das **2,4GHz**- und für
das **5GHz-Band** jeweils eine **eigene MAC-Adresse**.


>Eine MAC-Adresse (unter Windows „Physische Adresse“ genannt) **kann per Software verändert werden**. Allerdings ist davon **dringend abzuraten**, da dies **gravierende Auswirkungen** auf das korrekte Zusammenspiel aller Netzwerkkomponenten haben kann!

Die **weltweit eindeutigen Adressen** werden in hexadezimaler Form angegeben und haben einen
einheitlichen Aufbau. Die **ersten drei Bytes kennzeichnen den Hersteller (Vendor)** und **die
zweiten drei Bytes sind die fortlaufende Nummer der Netzwerkkarte**. Die folgende Tabelle zeigt
zwei Beispiele zu Produkten der Firmen ASUS und Intel. Ein Hersteller kann auch **mehrere
Vendorcodes** besitzen.

Die Kennziffern des Herstelleranteils werden von der IEEE zentral vergeben. Die Kennung der einzelnen Karten wird dann von den Herstellern selbst
vorgenommen.

| **Hersteller** | **Fortlauf. Nr** |  
|---|---|
| 10 BF 48 | B3 C1 92|
| 00 27 10 | 63 9B CD|

Für die erwartete hohe Anzahl von Geräten beim Internet der Dinge (IoT) wird die Länge von **48-Bit** für die MAC-Adresse auf Dauer **nicht reichen**. Nach RFC 4291 soll in Zukunft für IPv6 die Länge der MAC-Adresse (Interface ID) auf **64 Bit** erweitert werden. IPv6 ist insgesamt **128 Bit** lange, wovon die ersten 64 Bit das sogenannte Präfix sind und die letzten 64 Bit der MAC-Adresse (Interface ID) entsprechen (Quelle & weitere Details: <a href="https://de.wikipedia.org/wiki/IPv6" target="_blank">https://de.wikipedia.org/wiki/IPv6</a>).

**Vergleich IP-Adresse – Telefonnummer**<br>
Netze setzen sich meistens aus mehreren Segmenten (Teilabschnitten) zusammen. Eine Telefonnummer besteht beispielsweise aus einer Vorwahl und einer Teilnehmernummer. Eine IPAdresse ist ähnlich aufgebaut und besteht aus den folgenden zwei Teilen, wobei die Abkürzung ID für Identifikation steht:
- Die **Netzwerk-ID** (bzw.Netz-ID) im vorderen linken Teil entspricht der Vorwahl und
kennzeichnet das entsprechende Netzwerk.
- Die **Host-ID** im hinteren rechten Teil kennzeichnet eine einzelne Netzwerk-Schnittstelle und
entspricht der Teilnehmernummer im Ortsnetz. Bei einer **Netzwerk-Adresse** enthält sie nur Nullen.

Entsprechend können Rechner **im selben Netzwerk direkt miteinander kommunizieren**. Dagegen
erfordert Kommunikation zwischen Netzwerken einen Vermittler (Router oder Gateways). Auf den Rechnern ist dafür die Adresse des Vermittlers als Standardgateway (auch Default-Route) angegeben. Um zu erkennen, **wo die Netzwerk-ID endet und die Host-ID beginnt**, muss bei der Bezeichnung von Netzwerken zusätzlich zur IP-Adresse zwingend eine sogenannte **Subnetzmaske** (auch Netzmaske) angegeben werden. Diese legt die **Größe eines Netzwerk-Segments** fest.

**Subnetzmaske**<br>
Die Subnetzmaske ist ein Bitmuster, das (von links nach rechts) Teile der IP-Adresse maskiert, um
den Übergang zwischen Netz-ID und Host-ID zu kennzeichnen. Binär betrachtet besteht eine
Netzmaske aus einer Folge von Einsen, die ab einer bestimmten Stelle zu einer Folge von Nullen wechselt. Dieser Wechsel gibt an, wie viele Bits zur Netzwerk-ID (Einsen) und zur Host-ID (Nullen)
gehören. Die Schreibweise erfolgt dabei ebenfalls im Dotted-Decimal-Format. 


---

Netzmasken werden oft auf der Basis von ganzen Oktetten gesetzt, was die folgenden Beispiele
zeigen:

|  | 1. Beispiel |2. Beispiel | 3. Beispiel  |
|---|---|---|---|
| **IP-Adresse** | 119.95.1.200 | 172.95.1.200 | 193.95.1.200  |
| **Netzmaske**| 255.0.0.0 | 255.255.0.0 | 255.255.255.0 |   
| **Netz-ID**  | 119  | 172.95 | 193.95.1 |
| **Host-ID**  | 95.1.200 | 1.200 | 200  |

- Eine **Netzwerk-Adresse** stellt die erste Adresse in einem Netzwerk-Segment dar. Die Host-ID besteht dabei nur aus Nullen. Die letzte Adresse ist die **Broadcast-Adresse**, deren Host-ID nur Einsen enthält.
- Ein Netzwerk wird durch die Netzwerk-Adresse zusammen mit der Netzmaske definiert. In einem IPv4-Netzwerk kennzeichnet die Netzwerk-Adresse die erste IP-Adresse, die Netzmaske legt dessen Größe fest.
- Im Grunde ist auf IP-Ebene jedes andere Netzwerk ein Subnetz (Erläuterungen siehe unten)
des Internets.

| Class | Netzwerk-ID  | Netzmaske | Anzahl Netzwerke | Anzahl Hosts  |
|---|---|---|---|---|
| A | **1** bis **126** | 255.0.0.0 | 126 | 16'777'214   |
| B | **128.0** bis **191.255** | 255.255.0.0   | 16'384 | 65'534   |
| C | **192.0.0** bis **223.255.255** | 255.255.255.0 | 2'097'152   | 254 |
| D | **224** bis **239** | 240.0.0.0 |-| Multicast only  |
| E | **240** bis **254** | 240.0.0.0 |-| Reserved|

Einige Adressbereiche dürfen **nicht** vergeben werden:
- die mit **0** beginnen (wegen der Default-Route)
- die mit **127** beginnen (sie sind reserviert für interne Funktionen, wie z. B. die **Loopback**-
Adresse 127.0.0.1, die auf jedem Rechner konfiguriert ist und die die eigene Adresse für das
virtuelle Netzwerkinterface des IP-Stacks darstellt)
- die mit **255** beginnen (für Broadcasts).

Jedes IPv4-Netzwerk verwendet die **erste Adresse** (0) für die Adressierung des Netzwerks (Netzwerk-Adresse) und die **letzte Adresse** (255) als **Broadcast-Adresse**. Ein Class-C-Netz kann daher keine 256, sondern **nur 254 Hosts** beinhalten.

### Private Netzwerke

Vielleicht ist Ihnen schon aufgefallen, dass viele Netzwerke foldende IP Ranges verwenden:
*    10.0.0.0        -   10.255.255.255  (10/8 prefix) - 16'777'216 host Adressen
*    172.16.0.0      -   172.31.255.255  (172.16/12 prefix) - 1'048'576 host Adressen
*    192.168.0.0     -   192.168.255.255 (192.168/16 prefix) -  65'536 host Adressen

Dies ist so, da die [Internet Assigned Numbers Authority](https://en.wikipedia.org/wiki/Internet_Assigned_Numbers_Authority) diese ranges für [private Netze](https://en.wikipedia.org/wiki/Reserved_IP_addresses) reserviert hat. Die meisten Router für private Haushalte, kommen mit einem vorkonfiguriertem 192.168.x.x Netz. In Büros oder Universitäten, stösst man eher mal auf die anderen Netze aus dem Bereich 10.x.x.x. oder 172.16.x.x, auch aus dem Grund, dass man mit einer VPN Connection nicht mit einem privaten Netz kollidiert.