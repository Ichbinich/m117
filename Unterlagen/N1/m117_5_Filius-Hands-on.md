# Hands-on mit Filius

## Was ist Filius?
Filius ist eine Lernanwendung und wurde entwickelt, um den Einstieg in die Netzwerktechnologie zu vereinfachen und unterstützen. Zielgruppe sind Schüler in der Sekundarstufe 1. Filius kann aber auch sehr gut als Einstiegssoftware bei den  Netzwerkmodulen des ersten Informatiker-Lehrjahrs genutzt werden. Mit Filius werden Lernaktivitäten ermöglicht, die insbesondere entdeckendes Lernen unterstützen sollen. 

## Aufgabenstellung
**Hands-on Setup:** kleines Netzwerk mit 4 Computern und einem Switch mit Filius erstellen. **Arbeitsauftrag:** Fragen elektronisch und direkt im PDF beantworten.

**Vorgehen:**

1.  Arbeitsauftrag (PDF mit Textfeldern) downloaden und öffnen

    - Laden Sie [DIESES PDF-Dokument](ressourcen/2-auftraege-LN/01/m117_netzwerk_einstieg_filius.pdf) runter. **Achtung:** Sie müssen dieses Dokument "downloaden" und mit einer Adobe-Anwendung öffnen - bitte **NICHT** im Browser bearbeiten. Wer das Dokument nicht runterlädt und im Browser öffnet, kann den eingefügten content anschliessend **NICHT** speichern.

    - Öffnen sie das "runtergeladene" PDF mit einer Adobe-Anwendung (Adobe Reader reicht). Dieses PDF beinhaltet Felder, die sie **elektronisch ausfüllen** und anschliessend **samt Inhalt speichern** können. 

<br>

2.  Filius-SW downloaden und bereitstellen

    - [HIER](https://www.lernsoftware-filius.de/Herunterladen) kann die Lernsoftware Filius kostenlos runterladen. Wählen sie dabei die richtige SW für ihr OS aus (Windows, Ubuntu, Mac). 
    - Starten sie die Filius-Anwendung
    
<br>

3. Arbeitsauftrag durchführen

    - Erstellen sie auf Filius die Lab-Umgebung gemäss den Anforderungen im Auftrag. 

    - Arbeiten sie den Auftrag Schritt für Schritt durch und beantworten sie dabei die Fragen (Text in die grauen Felder des Auftrages eingeben). 

    - Wenn alle Fragen beantwortet sind, speichern sie ihr Dokument als **IhrNachname_Filius_1-Auftrag.pdf** ab. 
    
    - Geben sie ihr Dokument nach Angaben der Lehrperson ab.