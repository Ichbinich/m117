# Unterlagen

In diesem Verzeichnis werden Arbeitsaufträge und Arbeiten nach Kompetenzniveau abgelegt. Die Unterlagen werden laufend ergänzt oder angepasst und können bei Bedarf genutzt werden.

## Arbeitsaufträge Nach Kompetenzen

### Niveau 1: "Basic Skills"
- [Heimnetzwerk kennenlernen und dokumentieren - Praxisteil](N1/m117_1_Heimnetzwerk_dokumentieren.md) -
  [(_Link zu Beispiele von Lernenden_)](N1/ressourcen/1-beispiele)
- [Verkabelungsarten, Ethernettechnologien](N1/m117_2-Kabel_Verkabelungsarten_Ethernettechnologien.md)
- [Netzwerkeinstellungen und Adressen - Theorie](N1/m117_3_Netzwerkeinstellungen_und_Adressen_Theorie.md)
- [Netzwerkeinstellungen und Adressen - Windows Hands-on (Vorbereitung LB2)](N1/m117_4_Netzwerkeinstellungen_und_Adressen_Praxis.md)

### Hands-on

**1. Auftrag**
- [Filius-Auftrag](N1/m117_5_Filius-Hands-on.md) (erste Anwendung mit der Lernsoftware **"Filius"**)

**2. Auftrag** 
- [Filius-Auftrag](N1/ressourcen/2-auftraege-LN/02) (Netzwerk für Kleinbetrieb - mit der Lernsoftware **"Filius"**)

**3. Auftrag** 
- [Filius-Auftrag](N1/ressourcen/2-auftraege-LN/03) (**Folgeauftrag**: Netzwerk für Kleinbetrieb - mit der Lernsoftware **"Filius"**)

<br>

---

### Niveau 2: "Advanced Skills"

- [Subnetting - Theorie _(Vertiefung)_](N2/m117_4_Subnetting.md)
- [Subnetting - Übung _(mit Excelsheet)_](N2/m117_4_subnetting_uebung.md)
- [Gruppen und Berechtigungen - _(Theorie)_](N2/m117_5_Gruppen_und_Berechtigungen.md)


### Hands-on

**1. Auftrag**
- [Subnetting im letzten Oktett](N2/ressourcen/2-auftraege-LN/P0/README.md) _(Veränderung der Subnetzmaske und dessen Auswirkungen)_

**2. Auftrag**
- [Filius-Auftrag](N2/ressourcen/2-auftraege-LN/P1/README.md) _(Netzwerk für ein mittelgrosses Unternehmen)_

**3. Auftrag** 
- [Filius-Auftrag](N2/ressourcen/2-auftraege-LN/P2/README.md) _(Subnetting: Herr des Rings)_


<br>

---

### Niveau 3: "Expert Skills"

**1. Auftrag** 
- [Filius-Auftrag](N3/ressourcen/01)

(Subnetting für KMU ohne Router - mit der Lernsoftware **"Filius"**)

Hier geht es um das Verständnis, dass Subnetze ohne Router funktionieren (wenn auch voneinander getrennt). Nutzvolles Vorwissen bzgl. **VLANs** und **Routing**.

<br>

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---
