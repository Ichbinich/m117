## Arbeitsauftrag
1. Auftrag durchlesen, Subnetze sinnvoll aufteilen und Fragen beantworten (einfache Doku erstellen)
2. Filius-Vorlage downloaden und PCs gemäss vorher erstellter Matrix konfigurieren und testen.

### Hands-on

**1. Auftrag**
- [Filius-Auftrag](m117_Filius_Mittelbetrieb_Subnetting.pdf)
(Arbeitsauftrag in Ruhe durchlesen)

**2. Filius Vorlage**
- [Filius-File](KMU_ca_100MA_Auftrag.fls) (Downloaden und öffnen)

<br>

---

> [⇧ **Zurück zu N3**](../../README.md)

---


> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---
